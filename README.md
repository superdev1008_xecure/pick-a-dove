## AngularJS Dating Site
Front-end

## Preview

![image](https://drive.google.com/uc?export=view&id=1c_LaebIIbI6tjVg7ta8i8JlSNRubRb10)
![image](https://drive.google.com/uc?export=view&id=1Knjqza4hyH0Eqv-dnGcTg0vXF-ebWSDg)
![image](https://drive.google.com/uc?export=view&id=1EDdcdBkK9zEXTbgSbON0HBLE7SQnNzju)
## Clone the Repo
```sh
git clone https://gitlab.com/superdev1008_xecure/pick-a-dove.git
cd pick-a-dove
npm install
```
